/*jshint esversion: 6 */
class GameScene extends Phaser.Scene {
    
  constructor() {
    super('GameScene');
  }

  preload() {
    this.load.image('Forest', '../Images/Forest1.png');

    this.load.image('bullet', '../Assets/YellowSoldier/smallGold.png');
    this.load.atlas('yellowSoldier', '../Assets/YellowSoldier/animations.png', '../Assets/YellowSoldier/animations.json');

    this.load.image('greenBullet', '../Assets/GreenSoldier/smallGreen.png');
    this.load.image('greenParachute', '../Assets/GreenSoldier/greenParachute.png');
    this.load.atlas('greenSoldier', '../Assets/GreenSoldier/animations.png', '../Assets/GreenSoldier/animations.json');
    this.load.image('greenGrenade', '../Assets/GreenSoldier/grenade.png');

    this.load.image('greenJet', '../Assets/GreenSoldier/greenJet.png');
    this.load.image('missile', '../Assets/GreenSoldier/greenMissile.png');
    this.load.image('bomb', '../Assets/GreenSoldier/greenBomb.png');
    this.load.atlas('burner', '../Assets/GreenSoldier/jetBurner.png', '../Assets/GreenSoldier/jetburner.json');
  }
  
  create() {
    var forest = this.add.image(0, 0, 'Forest').setOrigin(0, 0);
    forest.setDisplaySize(game.scale.width, game.scale.height);

    //-------------PLAYER ASSETS TEXT-----------------------
    this.greenText = this.add.text(65, 48, 'GO!'); //{color: '#00ff00'}).setOrigin(0.5, 0);
    this.add.image(35, 50, 'greenSoldier').setScale(0.20);
    this.add.image(35, 100, 'greenJet').setScale(0.10);
    
    //-----------------ADD GREEN SOLDIER---------------------
    var greenSoldiers = new GreenSoldiers(this);
    this.addGreenSoldier(greenSoldiers); // add solidier at the beginning
    this.untilGreen = this.time.addEvent({delay: 8000, 
                                                callback: this.addGreenSoldier, 
                                                args: [greenSoldiers], 
                                                //callbackScope: this, 
                                                loop: true });

    //---------------ADD YELLOW SOLDIER ---------------------
    var yellowSoldiers =  new YellowSoldiers(this);
    //yellowSoldiers.setCollisionGroup();
    this.time.addEvent({delay: Phaser.Math.Between(200, 8000), 
                        callback: this.addEnemy, 
                        args: [yellowSoldiers],
                        repeat: 4});

    //this.physics.add.collider(yellowSoldiers, greenSoldiers, 
    //  function(yellowSoldiers, greenSoldiers) {
    //    //console.log("COLLISION");
    //  });
    
    //---------------------AIR STRIKE--------------------------
    var greenJet = new GreenJet(this, -50, 100);
    this.input.keyboard.on('keydown-A', function(event) {
      //prob need 3 - 5 secon delay
      greenJet.airStrike();
    });

    this.input.keyboard.on('keydown-SPACE', function(event) {
      greenJet.dropBombs();
    });

    //this.input.on('pointerdown', (pointer) => {
    //var  = Phaser.Math.Between(375, 550);
    //var timedEvent = 
    
  }

  update() {
    this.greenText.setText(this.untilGreen.getProgress().toString().substr(0, 4));

    //overlap
    //this.physics.add.collide(YellowSoldiers, GreenBullets, this.beenHit, null, this);

  }

  addEnemy(yellowSoldiers) {
    var x = 1250;
    var y = Phaser.Math.Between(375, 550);
    yellowSoldiers.addYellowSoldier(x, y);
  }

  addGreenSoldier(greenSoldiers) {
    var x = Phaser.Math.Between(150, 325);
    greenSoldiers.addGreenSoldier(x, 0);
  }

  beenHit() {
    console.log("BEEN HIT");
  }

}