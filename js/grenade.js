/*jshint esversion: 6 */
class Grenade extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'grenade');
    }

    fire (x, y) {
        this.body.reset(x, y);
        this.setActive(true);
        this.setVisible(true);
        this.setScale(0.08);
        this.setVelocityX(150);
    }

    preUpdate (time, delta) {
        super.preUpdate(time, delta);
        if (this.y <= -32) {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}
