var physicsConfig = {
    default: 'arcade',
    arcade: {
      debug: false
    }
  };
  

  var config = {
    autoCenter: true,
    type: Phaser.AUTO,
    parent: "game",
    physics: physicsConfig,
    width: 1250,
    height: 600,
    scene: [ GameScene ]
  };

  var game = new Phaser.Game(config);