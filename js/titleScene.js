/*jshint esversion: 6 */

class TitleScene extends Phaser.Scene {
    constructor() {
        super('TitleScene');
    }

    preload() {
        this.load.image('background', '../Images/titleImage.png');
    }

    create() {
        this.add.image(0, 0,'background').setOrigin(0, 0)
        .setDisplaySize(game.scale.width, game.scale.height);

        this.add.text(400, 50, 'LOADING....', {color: '#00ff00'}).setOrigin(0.5, 0);

        this.time.addEvent({
            delay: 4000,
            loop: false,
            callback: () => {
                this.scene.start("GameScene");
            }
        });
    }

    update() {

    }
}
/*
var TitleScene = new Phaser.Class({
    Extends: Phaser.Scene,
    initialize: function() {
        Phaser.Scene.call(this, { "key": "TitleScene" });
    },
    init: function() {},
    preload: function() {
        this.load.image('background', '../Images/wings.png');
		this.load.image('logo', '../Images/rangerRick.png');
    },
    create: function() {
        this.add.image(625,400,'background');
        this.add.image(625,500, 'logo');

        // WHEN PLAY BUTTON IS CLICKED GO TO FOREST SCENE
        this.time.addEvent({
            delay: 3000,
            loop: false,
            callback: () => {
                this.scene.start("ForestScene");
            }
        });
    },
    update: function() {}
});
*/