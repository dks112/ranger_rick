/*jshint esversion: 6 */
class GreenParachutes extends Phaser.Physics.Arcade.Group {
    constructor (scene) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 1,
            key: 'greenParachute',
            active: false,
            visible: false,
            classType: GreenBullet
        });
    }

    fireBullet (x, y) {
        let greenBullet = this.getFirstDead(false);
        if (greenBullet) {
            greenBullet.fire(x, y);
        }
    }
}