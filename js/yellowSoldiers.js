/*jshint esversion: 6 */
class YellowSoldiers extends Phaser.Physics.Arcade.Group {
    constructor (scene) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 7,
            key: 'yellowSoldier',
            active: false,
            visible: false,
            classType: YellowSoldier
        });

    }

    addYellowSoldier(x, y) {
        let yellowSoldier = this.getFirstDead(false);
    
        if(yellowSoldier) {
            yellowSoldier.addSoldier(x, y);
        }
    }

        /*
        yellowGroup.children.iterate(function (yellowSoldier) {
            if (yellowSoldier.x < 100) {
                yellowGroup.killAndHide(yellowSoldier);
                console.log(alert('Game Over.'));
            } 
            if(yellowSoldier.anims.getName() == 'walk'){
                console.log('Im walking here!');
                stopAndShoot(yellowSoldier);
                //Phaser.Actions.IncX(yellowGroup.getChildren(), -1);
            } 
        });
        */
    
}









/*
    yellowGroup = this.add.group({
        defaultKey: 'yellowSoldier',
        maxSize: 10,
        createCallback: function (yellowSoldier) {
            //yellowSoldier.setName('yellowSoldier' + this.getLength());
            //console.log('Created', yellowSoldier.name);
        },
        removeCallback: function (yellowSoldier) {
            //console.log('Removed', yellowSoldier.name);
        }
      });
      this.time.addEvent({delay: 500, loop: true, callback: addYellowSoldier});
  
}

function activateAlien (yellowSoldier) {
    yellowSoldier
    .setActive(true)
    .setVisible(true)
    .setTint(Phaser.Display.Color.RandomRGB().color)
    .setScale(0.28)
    .play('walk');
}

function addYellowSoldier () {
    // Random position above screen
    const x = Phaser.Math.Between(250, 800);
    const y = Phaser.Math.Between(-64, 0);

    // Find first inactive sprite in group or add new sprite, and set position
    const yellowSoldier = group.get(x, y);

    // None free or already at maximum amount of sprites in group
    if (!yellowSoldier) return;

    activateAlien(yellowSoldier);
}


/*
class YellowSoldier extends Phaser.GameObjects {
    //config is an object
    constructor(config) {
        // can pass in key instead of hard code
        super(config.scene, config.x, config.y, 'yellowSoldier');
        config.scene.add.existing(this);
        this.scene.physics.add.existing(this);
    }



        this.play('walk').setVelocityX(-60);
        
        if (this.anims.getName() == 'walk') {
            this.chain(['aim', 'shoot', 'lower', 'walk', 
                        'aim', 'shoot', 'lower', 'walk',
                        'aim', 'shoot', 'lower', 'walk',
                        'aim', 'shoot', 'lower', 'walk', 
                        'aim', 'shoot', 'lower', 'walk',
                        'aim', 'shoot', 'lower', 'walk']);      
                        //console.log(alert(2));           
        }
        //this.stopAndShoot();
    }

    stopAndShoot() {
        this.setVelocityX(0);
        var timedEvent = this.time.delayedCall(5000, this.stopAndShoot, [], this);
    }

    getPosition() {
        this.body.reset(x);
        return(x);
    }
    addYellowSoldier() {

    }
}

*/