/*jshint esversion: 6 */
var physicsConfig = {
    default: 'arcade',
    arcade: {
      debug: false
    }
  };

var config = {
    autoCenter: true,
    type: Phaser.AUTO,
    width: 1200,
    height: 600,
    physics: physicsConfig,
    parent: 'phaser-example',
    pixelArt: true,
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var yellowGroup;

var caption;

var captionStyle = {
    fill: '#FFFFFF',
    fontFamily: 'monospace',
    lineSpacing: 4
};

var captionTextFormat = (
    'Total:    %1\n' +
    'Max:      %2\n' +
    'Active:   %3\n' +
    'Inactive: %4\n' +
    'Used:     %5\n' +
    'Free:     %6\n' +
    'Full:     %7\n'
);

var game = new Phaser.Game(config);

function preload () {
    this.load.image('Forest', '../Images/Forest1.png');
    this.load.atlas('yellowSoldier', '../Assets/yellowSoldier/animations.png', 
        '../Assets/YellowSoldier/animations.json');
    this.load.image('bullet', '../Assets/yellowSoldier/smallGold.png');
}

function create () {
    
    this.anims.create({
        key: 'walk',
        frames: this.anims.generateFrameNames('yellowSoldier', {
            start: 0,
            end: 14,
            zeroPad: 2,
            prefix: 'walk-',
            suffix: '.png'
        }),
        setVelocityX: -60,
        frameRate: 10,
        repeat: 0
    });
    this.anims.create({
        key: 'aim',
        frames: this.anims.generateFrameNames('yellowSoldier', {
            start: 0,
            end: 4,
            zeroPad: 1,
            prefix: 'aim-',
            suffix: '.png'
        }),
        frameRate: 10,
        repeat: 0
    });
    this.anims.create({
        key: 'shoot',
        frames: this.anims.generateFrameNames('yellowSoldier', {
            start: 0,
            end: 4,
            zeroPad: 1,
            prefix: 'shoot-',
            suffix: '.png'
        }),
        frameRate: 10,
        repeat: 0
    });
    this.anims.create({
        key: 'lower',
        frames: this.anims.generateFrameNames('yellowSoldier', {
            start: 0,
            end: 4,
            zeroPad: 1,
            prefix: 'lower-',
            suffix: '.png'
        }),
        setVelocityX: 0,
        frameRate: 10,
        repeat: 0
    });

    this.add.image(0, 0, 'Forest').setOrigin(0, 0).setDisplaySize(game.scale.width, game.scale.height);
    //this.add.sprite(1000, )
    yellowGroup = this.add.group({
        defaultKey: 'yellowSoldier',
        maxSize: 10,
        //A function to be called when adding or creating group members
        createCallback: function (yellowSolder) {
            yellowSolder.setName('yellowSoldier' + this.getLength());
            console.log('Created', yellowSolder.name);
        },
        // A function to be called when removing group members
        removeCallback: function (yellowSoldier) {
            console.log('Removed', yellowSoldier.name);
        }
    });
  
    caption = this.add.text(16, 16, '', captionStyle);

    this.time.addEvent({
        delay: 400,
        loop: true,
        callback: addSoldier
    });
}

function update () {
    Phaser.Actions.IncX(yellowGroup.getChildren(), -1);

    yellowGroup.children.iterate(function (yellowSoldier) {
        if (yellowSoldier.x < 100) {
            yellowGroup.killAndHide(yellowSoldier);
            console.log(alert('Game Over.'));
        } 
        if(yellowSoldier.anims.getName() == 'walk'){
            console.log('Im walking here!');
            stopAndShoot(yellowSoldier);
            //Phaser.Actions.IncX(yellowGroup.getChildren(), -1);
        } 
    });

/*
    
    else if(yellowSoldier.anims.getName() == 'shoot') {
      //this.shootBullet();
      //this.events.on('addImage', this.addEvents, this )
      //var timedEvent = this.time.once({ delay: 1000, callback: onEvent, callbackScope: this });
      //var timedEvent = this.time.delayedCall(500, onEvent, [], this);
    }
    else {

    }
 */   

    caption.setText(Phaser.Utils.String.Format(captionTextFormat, [
        yellowGroup.getLength(),
        yellowGroup.maxSize,
        yellowGroup.countActive(true),
        yellowGroup.countActive(false),
        yellowGroup.getTotalUsed(),
        yellowGroup.getTotalFree(),
        yellowGroup.isFull()
    ]));
}

function activateAlien (yellowSoldier) {
    yellowSoldier
    .setActive(true)
    .setVisible(true)
    //.setTint(Phaser.Display.Color.RandomRGB().color)
    .setScale(0.28)
    .play('walk');
    if (yellowSoldier.anims.getName() == 'walk') {
        yellowSoldier.chain(['aim', 'shoot', 'lower', 'walk', 
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk', 
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk', 
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk',
                    'aim', 'shoot', 'lower', 'walk']); 
    }
}

function addSoldier() {
    // Random position above screen
    const x = Phaser.Math.Between(2000, 1200);
    const y = Phaser.Math.Between(425, 500);

    // Find first inactive sprite in group or add new sprite, and set position
    const yellowSoldier = yellowGroup.get(x, y);

    // None free or already at maximum amount of sprites in group
    if (!yellowSoldier) return;

    activateAlien(yellowSoldier);
}

function stopAndShoot (yellowSoldier) {
    yellowSoldier.setVelocityX(0);
}