/*jshint esversion: 6 */
class GreenBullets extends Phaser.Physics.Arcade.Group {
    constructor (scene) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 50,
            key: 'greenBullet',
            active: false,
            visible: false,
            classType: GreenBullet
        });
    }

    fireBullet (x, y) {
        let greenBullet = this.getFirstDead(false);
        if (greenBullet) {
            greenBullet.fire(x, y);
        }
        //console.log('fired bullet');
    }
}