/*jshint esversion: 6 */
class GreenSoldiers extends Phaser.Physics.Arcade.Group {
    constructor (scene) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 1,
            key: 'greenSoldier',
            active: false,
            visible: false,
            classType: GreenSoldier
        });
    }

    addGreenSoldier(x, y) {
        let greenSoldier = this.getFirstDead(false);
    
        if(greenSoldier) {
            greenSoldier.addSoldier(x, y);
        }
    }
}
