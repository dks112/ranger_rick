/*jshint esversion: 6 */
class GreenSoldier extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'greenSoldier');
        //--------------------GREEN SOLDIER ANIMS-------------------------
        this.anims.create({
            key: 'shoot',
            frames: this.anims.generateFrameNames('greenSoldier', {
                start: 0,
                end: 9,
                zeroPad: 2,
                prefix: 'shoot-',
                suffix: '.png'
            }),
            frameRate: 8,
            repeat: 25
        });
        
    }

    addSoldier(x, y) {
        //console.log("X Before reset: " + x);
        this.body.reset(x, y); //resynchronize the Body with its Game Object
        //console.log("Green at X: " + x);
        this.greenBullets = new GreenBullets(this.scene);
        this.greenParachute = new GreenParachute(this.scene, x - 35, -110); // issue this man a parachute
        this.land = Phaser.Math.Between(350, 550); // level of height to land
        //greenParachute.deployChute(50, 50);
        this.setActive(true);
        this.setVisible(true);
        this.setScale(0.5);
        // A Game Object with a higher depth value will always render in front of one with a lower value.
        this.depth = this.land;
        this.play('shoot');
        this.j = 0;
        
    }

    shootBullet() {
        //console.log('Shoot Bullet');
        this.j++;
        if((this.j % 6) == 0) { // 6FPS
            this.greenBullets.fireBullet(this.x + 70, this.y);
        }
    }

    preUpdate (time, delta) {
        //super preUpdate here????
        //console.log(this.anims.currentFrame.index);
        if(this.anims.currentFrame.index == 3) {
            this.shootBullet();
        }
        
        if (this.y < this.land) {
            this.y++;
            this.greenParachute.y = (this.y - 110);
        } else {
            //destory()?
            this.greenParachute.setVisible(false);
        }
        //if (this.x <= 0) {
        //    this.setActive(false);
        //    this.setVisible(false);
            //console.log(alert('GAME OVER'));
        //}
        super.preUpdate(time, delta);
    }
}

//var greenParachute;