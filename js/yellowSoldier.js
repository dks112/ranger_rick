/*jshint esversion: 6 */
class YellowSoldier extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'yellowSoldier');
        //config.scene.add.existing(this);
        //--------------------YELLOW SOLDIER-----------------------
        this.anims.create({
            key: 'walk',
            frames: this.anims.generateFrameNames('yellowSoldier', {
                start: 0,
                end: 14,
                zeroPad: 2,
                prefix: 'walk-',
                suffix: '.png'
            }),
            frameRate: 10,
            repeat: 0
        });
        this.anims.create({
            key: 'aim',
            frames: this.anims.generateFrameNames('yellowSoldier', {
                start: 0,
                end: 4,
                zeroPad: 1,
                prefix: 'aim-',
                suffix: '.png'
            }),
            frameRate: 10,
            repeat: 0
        });
        this.anims.create({
            key: 'shoot',
            frames: this.anims.generateFrameNames('yellowSoldier', {
                start: 0,
                end: 4,
                zeroPad: 1,
                prefix: 'shoot-',
                suffix: '.png'
            }),
            frameRate: 10,
            repeat: 0
        });
        this.anims.create({
            key: 'lower',
            frames: this.anims.generateFrameNames('yellowSoldier', {
                start: 0,
                end: 4,
                zeroPad: 1,
                prefix: 'lower-',
                suffix: '.png'
            }),
            setVelocityX: 0,
            frameRate: 10,
            repeat: 0
        });
    }

    addSoldier(x, y) {
        globalThis.bullets = new Bullets(this.scene);
        this.body.reset(x, y); //resynchronize the Body with its Game Object
        this.setActive(true);
        this.setVisible(true);
        this.setVelocityX(-60);
        this.setScale(0.25);
        this.play('walk');
        for(var i = 0; i < 15; i++) { // also number of shots/bullets
            this.chain('aim');
            this.chain('shoot');
            this.chain('lower');
            this.chain('walk');
        }
        globalThis.i = 0;
    }

    shootBullet() {
        i++;
        //console.log(i);
        if((i % 6) == 0) { // 6FPS
            bullets.fireBullet(this.x - 70, this.y);
        }
    }

    preUpdate (time, delta) {
        
        if(this.anims.getName() == 'walk') {
            this.setVelocityX(-60);
        } else if (this.anims.getName() == 'shoot') {
            this.setVelocityX(0);
            if(this.anims.currentFrame.index == 4) {
                this.shootBullet();
            }
        } else {
            this.setVelocityX(0);
        }

        super.preUpdate(time, delta);
        if (this.x <= 0) {
            this.setActive(false);
            this.setVisible(false);
        }
    }
}