/*jshint esversion: 6 */
class GreenParachute extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, 'greenParachute');
        console.log("PARACHUTE");
        scene.add.existing(this);
        this.setActive(true);
        this.setVisible(true);
        this.setScale(0.20);
    }

    deployChute (x, y) {
        //this.body.reset(x, y);
        this.setActive(true);
        this.setVisible(true);
        this.setScale(0.8);
        //this.setVelocityY(150);
    }

    preUpdate (time, delta) {
        super.preUpdate(time, delta);
        //if (this.y >= 500) {
        //    this.setActive(false);
        //    this.setVisible(false);
        //}
    }
}
