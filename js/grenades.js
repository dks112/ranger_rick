/*jshint esversion: 6 */
class Grenade extends Phaser.Physics.Arcade.Group {
    constructor (scene) {
        super(scene.physics.world, scene);

        this.createMultiple({
            frameQuantity: 50,
            key: 'grenade',
            active: false,
            visible: false,
            classType: Grenade
        });
    }

    fireBullet (x, y) {
        let grenade = this.getFirstDead(false);
        if (grenade) {
            grenade.fire(x, y);
        }
        //console.log('fired bullet');
    }
}